<?php
namespace Ciebit\Requisicoes;

class Requisicao
{
    private $post;
    private $get;

    public function __construct()
    {
        $this->get = $_GET;
        $this->post = $_POST;
    }

    public function obterGet(string $indice)
    {
        if (isset($this->get[$indice])) {
            return $this->get[$indice];
        }

        return null;
    }

    public function obterPost(string $indice)
    {
        if (isset($this->post[$indice])) {
            return $this->post[$indice];
        }

        return null;
    }
}
